'use strict';

const { body } = require('express-validator');

const instrumentValidator = [
    body('instrumentName').notEmpty().trim().escape(),
    body('slug').notEmpty().trim().escape(),
    body('description').trim().escape(),
    body('owner').notEmpty().trim().escape(),
    body('purpose').trim().escape(),
    body('startDate').notEmpty().trim().escape(),
    body('durationAmount').notEmpty().isNumeric().escape(),
    body('durationTime').notEmpty().trim().escape(),
    // body('task').notEmpty().trim(),
    body('complianceRequirements').notEmpty().trim().escape(),
    body('sampleUnit').notEmpty().trim().escape(),
    body('sampleRate').notEmpty().trim().escape(),
    body('contextualAttributes').notEmpty().trim().escape(),
    body('environments').notEmpty().trim().escape()
];

module.exports = {
    instrumentValidator
};