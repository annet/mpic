'use strict';

const expect = require('chai').expect;
const salUtil = require('../../util/salUtil');

describe('salUtil', () => {
    it('A new SAL entry is generated properly', () => {
        const now = new Date();
        const expectedValue = '\n* ' + now.getHours().toString().padStart(2, '0') + ':' + now.getMinutes().toString().padStart(2, '0') +
        ' the creator: A new instrument has been created - the-instrument-name';
        const actualValue = salUtil.getNewSalEntry('the creator', 'the-instrument-name');

        expect(actualValue).equal(expectedValue);
    });
});
