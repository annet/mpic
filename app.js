'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const logger = require('pino')();

const instruments = require('./routes/instruments');
const config = require('./config/configuration');
const database = require('./config/database');

const app = express();
app.use(bodyParser.json({ limit: config.conf.service.max_body_size || '100kb' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static(__dirname + '/public'));

app.use(function (req, res, next) {
    req.app = {};
    req.app.db = database.db;
    req.app.conf = config.conf;
    req.app.logger = logger;
    next();
});

app.use('/', instruments);

module.exports = {
    logger,
    config,
    app
}

