CREATE DATABASE IF NOT EXISTS mpic;

GRANT ALL PRIVILEGES ON mpic.* TO 'maria'@'%';

USE mpic;

CREATE TABLE IF NOT EXISTS instruments (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    slug VARCHAR(255) NOT NULL,
    description TEXT,
    creator VARCHAR(255) NOT NULL,
    owner VARCHAR(255) NOT NULL,
    purpose VARCHAR(255),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    start_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    end_date TIMESTAMP,
    task VARCHAR(1000) NOT NULL,
    compliance_requirements SET('legal', 'gdpr') NOT NULL,
    sample_unit VARCHAR(255) NOT NULL,
    sample_rate FLOAT NOT NULL,
    environments SET('development', 'staging', 'production', 'external') NOT NULL,
    security_legal_review VARCHAR(1000) NOT NULL,
    status VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS instrument_sample_rates (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    instrument_id INT UNSIGNED NOT NULL,
    dblist VARCHAR(255) NOT NULL,
    sample_rate FLOAT NOT NULL,
    constraint fk_de_type
    foreign key(instrument_id)
    references instruments (id)
);

CREATE TABLE IF NOT EXISTS contextual_attributes (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    contextual_attribute_name VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS instrument_contextual_attribute_lookup (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    instrument_id INT UNSIGNED NOT NULL,
    contextual_attribute_id INT UNSIGNED NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO contextual_attributes (contextual_attribute_name) VALUES
('agent_app_install_id'),
('agent_client_platform'),
('agent_client_platform_family'),
('page_id'),
('page_title'),
('page_namespace'),
('page_namespace_name'),
('page_revision_id'),
('page_wikidata_id'),
('page_wikidata_qid'),
('page_content_language'),
('page_is_redirect'),
('page_user_groups_allowed_to_move'),
('page_user_groups_allowed_to_edit'),
('mediawiki_skin'),
('mediawiki_version'),
('mediawiki_is_production'),
('mediawiki_is_debug_mode'),
('mediawiki_database'),
('mediawiki_site_content_language'),
('mediawiki_site_content_language_variant'),
('performer_is_logged_in'),
('performer_id'),
('performer_name'),
('performer_session_id'),
('performer_pageview_id'),
('performer_groups'),
('performer_is_bot'),
('performer_language'),
('performer_language_variant'),
('performer_can_probably_edit_page'),
('performer_edit_count'),
('performer_edit_count_bucket'),
('performer_registration_dt');

INSERT INTO instruments (name, slug, description, creator, owner, purpose, task, compliance_requirements, sample_unit, sample_rate, environments, security_legal_review, status) VALUES
('Web Scroll UI', 'web-scroll-ui', 'abc description', 'Jane Doe', 'Web Team', 'purpose 1', 'task 1', ('legal'), 'pageview', 0.25, ('development'), 'reviewed', 'off'),
('Desktop UI Interactions', 'desktop-ui-interactions', 'efd description', 'Jill Hill', 'Editing Team', 'purpose 1', 'task 1', ('gdpr'), 'session', 0.5, ('staging'), 'reviewed', 'on'),
('Android Article Find In Page', 'android-article-find-in-page', 'xyz description', 'Julie Doe', 'Android Team', 'purpose 1', 'task 1', ('gdpr,legal'), 'pageview', 1.0, ('production'), 'pending', 'off'),
('Android Article Link Preview Interaction', 'android-article-link-preview-interaction', 'www description', 'Jim Doe', 'Android Team', 'purpose 1', 'task 1', ('gdpr,legal'), 'pageview', 0.5, ('production'), 'reviewed', 'on'),
('Android Article TOC', 'android-article-toc', 'eee description', 'John Doe', 'Android Team', 'purpose 1', 'task 1', ('legal'), 'pageview', 1.0, ('development'), 'under review', '0ff');
