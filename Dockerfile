FROM docker-registry.wikimedia.org/nodejs18-devel as base
WORKDIR /src
COPY package*.json ./
EXPOSE 8080

FROM base as development
ENV NODE_ENV=development
RUN npm ci
CMD ["node", "server.js"]
