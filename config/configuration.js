'use strict';

const yaml = require('js-yaml');
const fs = require('fs');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const logger = require('pino')();

// Load configuration properties
let configFile = 'config.dev.yaml';
const argv = yargs(hideBin(process.argv)).argv;
if (argv.config !== undefined) {
	configFile = argv.config;
}
const config = yaml.load(fs.readFileSync(configFile, 'utf8'));

// Load secrets as enviroment variables
if (process.env.DATABASE_PASSWORD) {
	config.database.password = process.env.DATABASE_PASSWORD;
} else {
	logger.error('Error while setting an env variable. There is no value for DATABASE_PASSWORD');
}
if (process.env.SAL_PASSWORD) {
	config.sal.password = process.env.SAL_PASSWORD;
} else {
	logger.error('Error while setting an env variable. There is no value for SAL_PASSWORD');
}
if (process.env.IDP_CLIENT_SECRET) {
	config.idp.client_secret = process.env.IDP_CLIENT_SECRET;
} else {
	logger.error('Error while setting an env variable. There is no value for IDP_CLIENT_SECRET');
}

exports.conf = config;
