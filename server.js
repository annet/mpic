'use strict';

const { app, config, logger } = require('./app');

app.listen(config.conf.service.port, function () {
    logger.info('mpic has started listening on port ' + config.conf.service.port);
});
