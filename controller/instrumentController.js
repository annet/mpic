'use strict';

const MWBot = require('mwbot');
const { getNewSalEntry, getCurrentSalHeader, getPageContent } = require('../util/salUtil');

const getInstruments = ((req, res) => {
	req.app.db.select().table('instruments')
		.then((data) => {
			req.app.logger.info('Instruments retrieved successfully from the database');
			res.send(data);
		})
		.catch((error) => {
			req.app.logger.error('Error while retrieving instruments from the database: ', error);
		});
});

const getInstrument = ((req, res) => {
	const instrumentSlug = req.params.slug;
	req.app.db('instruments')
		.where({
			slug: instrumentSlug
		})
		.select('*')
		.then((data) => {
			if (data.length) {
				req.app.logger.info('Instrument ' + instrumentSlug + ' retrieved successfully from the database');
				res.json(data[0]);
			}
		})
		.catch((err) => res.status(400).json('Instrument not found'));
});

const addInstrument = ((req, res) => {
	const createdAt = new Date();
	const updatedAt = new Date();
	const startDate = new Date(Date.parse(req.body.startDate));
	const endDate = new Date(Date.parse(req.body.startDate));
	let amount = Number(req.body.durationAmount);
	if (req.body.durationTime === 'Weeks') {
		amount *= 7;
	}
	endDate.setDate(endDate.getDate() + amount);

	// FIXME Get the creator (the logged user?)
	const creator = 'me';

	// Register instrument
	// FIXME The creator should be logged user. For now it's hardcoded as 'me'
	// FIXME We don't have field for 'task'. For now it's hardcoded
	const data = {
		name: req.body.instrumentName,
		slug: req.body.slug,
		description: req.body.description,
		creator: creator,
		owner: req.body.owner,
		purpose: req.body.purpose,
		task: 'task',
		created_at: createdAt,
		updated_at: updatedAt,
		start_date: startDate,
		end_date: endDate,
		compliance_requirements: req.body.complianceRequirements,
		sample_unit: req.body.sampleUnit,
		sample_rate: req.body.sampleRate,
		contextual_attributes: req.body.contextualAttributes,
		environments: req.body.environments
	};

	req.app.db('instruments')
		.insert(data)
		.then(() => {
			req.app.logger.info('Instrument registered successfully in the database');

			// Add an entry to the SAL
			const bot = new MWBot();
			bot.loginGetEditToken({
				apiUrl: req.app.conf.sal.api_url,
				username: req.app.conf.sal.username,
				password: req.app.conf.sal.password
			}).then(() => {
				return bot.read(req.app.conf.sal.page);
			}).then((response) => {
				// Get current SAL content
				let pageContent = getPageContent(response);
				// Prepare the new SAL entry
				const currentDayHeader = getCurrentSalHeader();
				if (!pageContent.includes(currentDayHeader)) {
					pageContent += currentDayHeader;
				}
				// Log the new entry to the SAL
				pageContent += getNewSalEntry(creator, req.body.instrumentName);
				return bot.update(req.app.conf.sal.page, pageContent);
			}).then((response) => {
				req.app.logger.info('Added an entry to SAL');
				res.redirect('/' + req.body.slug);
			}).catch((error) => {
				req.app.logger.error('An error has found while logging to SAL: ' + error);
			});
		})
		.catch((error) => {
			req.app.logger.error('Error while registering the instrument: ' + error);
		});
});

module.exports = {
	addInstrument,
	getInstrument,
	getInstruments
};
